import lab3
import random
import matplotlib.pyplot as plt
import matplotlib.ticker as tcr
import cProfile
import pstats
from pstats import SortKey

start, stop, step = 10, 100, 10  # Данные для цикла заполнения массивов данных

# Заполнение массива со словарями
mass_dict = [{i + 1: [random.randint(1, n) for _ in range(3)] for i in range(n)}
             for n in range(start, stop + 1, step)]

# Заполнение массива с вершинами А и Б
mass_vertex = [(random.randint(1, len(elem)), random.randint(1, len(elem))) for elem in mass_dict]

# Заносим кол-во элементов участвующих в сортировке для построения графика
mass_qnt_in = [len(mass_dict[_]) for _ in range(len(mass_dict))]

mass_qnt_iterations = []

i = 0

print('Подсчет кол-ва операций...')

for elem in mass_dict:
    cProfile.run('lab3.minimum_path(elem, mass_vertex[i][0], mass_vertex[i][1])', 'stats.log')

    with open('output.txt', 'w') as log_file_stream:
        p = pstats.Stats('stats.log', stream=log_file_stream)
        p.strip_dirs().sort_stats(SortKey.NAME == 'swap').print_stats()

    with open('output.txt') as f:
        lines = [line.strip() for line in f]

    qnt_iterations = [line for line in lines if line not in '' and line.count('swap')]

    mass_qnt_iterations.append(int(qnt_iterations[0].split()[0]))

    i += 1

print('Строим график...')

# График кол-ва операций
ax = plt.subplot()
plt.plot(mass_qnt_in, mass_qnt_iterations, marker='o', ms=3)
plt.title('График кол-ва операций')
plt.xlabel('Кол-во вершин')
plt.ylabel('Кол-во операций')
ax.set_xlim(mass_qnt_in[0], mass_qnt_in[-1])
ax.xaxis.set_major_locator(tcr.MultipleLocator(step))
plt.grid()
plt.show()
