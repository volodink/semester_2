def swap_search_path(a, b):
    if a < b:
        return True


# Наивный поиск кратчайшего пути из точки А в точку Б
def minimum_path(graph, vertex_a, vertex_b):
    shortest_path = {vertex: len(graph) for vertex in graph}
    shortest_path[vertex_a] = 1
    queue = [vertex_a]
    while queue:
        vertex = queue.pop(0)
        for neighbour in graph[vertex]:
            min_path = shortest_path[vertex] + 1
            if swap_search_path(min_path, shortest_path[neighbour]):
                shortest_path[neighbour] = min_path
                queue.append(neighbour)
    return shortest_path[vertex_b], shortest_path[vertex_b] - 1
