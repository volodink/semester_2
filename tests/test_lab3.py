from src.lab_3 import lab3
import pytest

# Данные графика №1
graph_1 = {
    1: [2, 8],
    2: [1, 3],
    3: [2, 5],
    4: [5],
    5: [3, 4, 6, 7],
    6: [5],
    7: [5, 8],
    8: [1, 7]
}

# Данные графика №2
graph_2 = {
    1: [2, 4],
    2: [1, 3, 4],
    3: [2, 5, 6],
    4: [1, 2],
    5: [3, 6],
    6: [3, 5, 7, 8],
    7: [6],
    8: [6]
}

# Данные графика №3
graph_3 = {
    1: [2, 5],
    2: [1, 3, 4],
    3: [2, 4, 7],
    4: [2, 3, 6],
    5: [1, 6],
    6: [4, 5, 7],
    7: [3, 6]
}

# Параметры тестов
test_param_cases = [
    (graph_1, 1, 6, (5, 4)),
    (graph_2, 2, 5, (3, 2)),
    (graph_3, 1, 7, (4, 3))
]


# Тест корректности поиска кратчайшего пути
@pytest.mark.parametrize("dict_in, vertex_a, vertex_b, var_out", test_param_cases)
def test_minimum_path_search(dict_in, vertex_a, vertex_b, var_out):
    assert lab3.minimum_path(dict_in, vertex_a, vertex_b) == var_out
